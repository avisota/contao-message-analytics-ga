<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-05-30T04:00:16+02:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message']['gaCampaign']['0']         = 'Num da la campagna';
$GLOBALS['TL_LANG']['orm_avisota_message']['gaCampaign']['1']         = 'Utilisà sco chavazzin per l\'analisa. Identifitgescha ina promoziun da products specifica u ina campagna strategica. Standard: <em>L\'object da l\'e-mail</em>. Exempel: <code>utm_campaign=vendita_da_primavaira</code>.';
$GLOBALS['TL_LANG']['orm_avisota_message']['gaEnable']['0']           = 'Activar Google analytics';
$GLOBALS['TL_LANG']['orm_avisota_message']['gaEnable']['1']           = 'Activar Google analytics per observar quests e-mails.';
$GLOBALS['TL_LANG']['orm_avisota_message']['gaTerm']['0']             = 'Term da la campagna';
$GLOBALS['TL_LANG']['orm_avisota_message']['gaTerm']['1']             = 'Utilisà per la tschertga pajada. Utilisescha utm_term per nudar il chavazzin per questa reclama. Exempel: <code>utm_term=chalzers+da+currer</code>.';
$GLOBALS['TL_LANG']['orm_avisota_message']['ga_campain_title']        = 'Campagna: %s';
$GLOBALS['TL_LANG']['orm_avisota_message']['google_analytics_legend'] = 'Google Analytics';


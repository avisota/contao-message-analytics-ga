<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-05-30T04:00:16+02:00
 */


$GLOBALS['TL_LANG']['orm_avisota_message']['gaCampaign']['0']         = 'Kampagnenname';
$GLOBALS['TL_LANG']['orm_avisota_message']['ga_campain_title']        = 'Kampagne: %s';
$GLOBALS['TL_LANG']['orm_avisota_message']['google_analytics_legend'] = 'Google Analytics';

